#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "wallyingLib.h"


#define DATA_NUM    (20)
#define DATA_SIZE   (100)

static uint8_t data_buf[DATA_SIZE] = {0x00};
static uint32_t data_len = 0;

int main(int argc, char *argv[])
{
    printf("Hello World!\r\n");

    while (1)
    {
        memset(data_buf, 0, sizeof(data_buf));

        data_len = rand_number(1, DATA_NUM);
        for (int i = 0; i < data_len; i++) {
            data_buf[i] = rand_number(0x00, 0xFF);
        }
        hexdump("data_buf", data_buf, data_len);

        array_reverse(data_buf, data_len);

        hexdump("data_buf -> array_reverse", data_buf, data_len);

        system("pause");
    }

    system("pause");
    return 0;
}
